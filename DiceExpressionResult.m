//
//  DiceExpressionResult.m
//  Game Master
//
//  Created by Tevis Money on 2/24/12.
//  Copyright (c) 2012 Apple. All rights reserved.
//

#import "DiceExpressionResult.h"

@implementation DiceExpressionResult

@synthesize workStack;
@synthesize result;

- (id)initWithResult:(NSNumber *) value workStack:(NSMutableArray *) stack {
    self = [super init];
    if (self) {
        workStack = [[NSArray alloc] initWithArray:stack];
        result = [[NSNumber alloc] initWithInt:[value intValue]];
    }
    return self;
}

+ (id) resultWithValue:(NSNumber *) value workStack:(NSMutableArray *) stack{
    return [[[self alloc] initWithResult:value workStack:stack] autorelease];
}


- (void)dealloc {
    self.workStack = nil;
    self.result = nil;
    [super dealloc];
}

@end
