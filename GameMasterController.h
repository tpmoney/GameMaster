//
//  GameMasterController.h
//  Game Master
//
//  Created by Tevis Money on 7/19/11.
//  Copyright 2011 Apple. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DiceRollerController.h"

@interface GameMasterController : NSObject <NSApplicationDelegate> {
    BOOL applicationHasStarted;    //Flag to indicate if the application loaded
    DiceRollerController *roller;
}

- (IBAction)showDiceRoller:(id)sender;

@end
