//
//  DungeonSet.m
//  Game Master
//
//  Created by Tevis Money on 10/30/10.
//  Copyright 2010 Apple. All rights reserved.
//
//  02/21/2012 - Added functionality for Dice Roller

#import "DungeonSet.h"
#import "DungeonArea.h"

@implementation DungeonSet

NSString * const kDungeonNotes = @"kDungeonNotes";
NSString * const kDungeonAreaArray = @"kDungeonAreaArray";
NSString * const kDungeonDataVersion = @"kDungeonDataVersion";



CGFloat const cMinimumLeftPaneSize = 200.0f;  //Minimum size of left hand panel

- (IBAction)addNewItemToAreaList:(id)sender{
    NSString *newItemName = [newAreaNameTextField stringValue];
    for(DungeonArea *existingItem in dungeonAreasArray){
        if ([[existingItem areaName]
             caseInsensitiveCompare:newItemName] == NSOrderedSame) {
            NSAlert *alert = [[NSAlert alloc] init];
            [alert addButtonWithTitle:@"Duplicate"];
            [alert addButtonWithTitle:@"Cancel"];
            [alert setMessageText:@"This area name already exists in your"\
             " area list."];
            [alert setInformativeText:@"Do you really want to add a "\
             "duplicate area?"];
            [alert setAlertStyle:NSWarningAlertStyle];
            
            int returnValue = [alert runModal];
            [alert release];
            if (returnValue != NSAlertFirstButtonReturn) {
                return;
            } else {
                break;
            }

        }
    }
    DungeonArea *itemToAdd = [DungeonArea
                                   dungeonAreaWithName:newItemName];
    [dungeonAreaController addObject:itemToAdd];
    if (sender == newAreaNameTextField) {
        [newAreaNameTextField setStringValue:@""];
    }
    [self updateChangeCount:NSChangeDone];
}

- (IBAction)removeSelectedItemFromAreaList:(id)sender{
    
    int selectedIndex = [dungeonAreaTableView selectedRow];
    if(selectedIndex == -1){
        return;
    }
    
    DungeonArea *itemToBeDeleted = [dungeonAreasArray objectAtIndex:selectedIndex];
    
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Delete"];
    [alert setMessageText:
        [NSString stringWithFormat:@"Delete the area %@?", 
         [itemToBeDeleted areaName]]];
    [alert setInformativeText:@"Deleted items cannot be restored."];
    [alert setAlertStyle:NSWarningAlertStyle];
    
    int returnValue = [alert runModal];
    
    if (returnValue == NSAlertFirstButtonReturn) {
        [alert release];
        return;
    }
    [alert release];

    [dungeonAreaController removeObjectAtArrangedObjectIndex:selectedIndex];
    [self updateChangeCount:NSChangeDone];
}

- (IBAction)exportAsText:(id)sender{
    
    NSSavePanel *savePanel = [NSSavePanel savePanel];
    [savePanel setTitle:@"Export to Text..."];
    [savePanel setPrompt:@"Export"];
    [savePanel setAllowedFileTypes:[NSArray arrayWithObject:@"txt"]];
    [savePanel setAllowsOtherFileTypes:YES];
    NSInteger result = [savePanel runModal];
    if (result == NSFileHandlingPanelOKButton) {
        
        //Format the output string for the text file
        NSMutableString *theOutput = [NSMutableString stringWithCapacity:512];
        [theOutput appendString:@"Title:\n"];
        [theOutput appendString:[self displayName]];
        [theOutput appendString:@"\n\n"];
        [theOutput appendFormat:@"Dungeon Notes: %@ \n\n", [dungeonNotes string]];
        for(DungeonArea *theArea in dungeonAreasArray){
            [theOutput appendFormat:@"Area Name: %@ \n\n", [theArea areaName]];
            [theOutput appendFormat:@"Description: %@ \n\n", 
                [[theArea description] string]];
            [theOutput appendFormat:@"Game Master's Notes: %@ \n\n", 
                [[theArea gameMasterNotes] string]];
            [theOutput appendFormat:@"Enemies Notes: %@ \n\n", 
             [[theArea enemiesNotes] string]];
        }
        
        //Write the string out, display an error if we get one.
        NSError *writeError;
        if (![theOutput writeToURL:[savePanel URL]
                        atomically:YES
                          encoding:NSUTF8StringEncoding
                             error:&writeError]){
            NSAlert *alert = [NSAlert alertWithError:writeError];
            [alert runModal];
        }
    }
    
}

- (IBAction)didChangeAreaName:(id)sender{
    [self updateChangeCount:NSChangeDone];
}


- (BOOL)displayVersioningErrorWithMessage:(NSString *)aMessage{
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"Don't Open"];
    [alert addButtonWithTitle:@"Open"];
    [alert setMessageText:aMessage];
    [alert setInformativeText:@"Opened files will be converted."];
    [alert setAlertStyle:NSWarningAlertStyle];
    
    int returnValue = [alert runModal];
    
    if (returnValue == NSAlertFirstButtonReturn) {
        [alert release];
        return NO;
    }
    
    [alert release];
    [self updateChangeCount:NSChangeDone];
    return YES;
}

#pragma mark -
#pragma mark NSSplitViewDelegate

- (CGFloat)splitView:(NSSplitView *)splitView
    constrainMinCoordinate:(CGFloat)proposedMin 
    ofSubviewAt:(NSInteger)dividerIndex{
    if (dividerIndex == 0) {
        return cMinimumLeftPaneSize;
    }
    return proposedMin;
}

#pragma mark -
#pragma mark NSDocument

- (id)init
{
    self = [super init];
    if (self) {
        
        //Initialize the version number
        dataVersion = [[NSNumber numberWithFloat: 1.0] retain ];
        
        
        dungeonAreasArray = [[NSMutableArray alloc] init];
        
        //Add 3 empty areas
        DungeonArea *area1 = [DungeonArea dungeonAreaWithName:@"Area 1"];
        [dungeonAreasArray addObject:area1];
        
        DungeonArea *area2 = [DungeonArea dungeonAreaWithName:@"Area 2"];
        [dungeonAreasArray addObject:area2];
        
        DungeonArea *area3 = [DungeonArea dungeonAreaWithName:@"Area 3"];
        [dungeonAreasArray addObject:area3];
        
        dungeonNotes = [[NSAttributedString alloc] initWithString:@""];
         

    }
    return self;
}

- (void) dealloc
{
    [dataVersion release];
    [dungeonAreasArray release];
    [dungeonNotes release];
    [super dealloc];
}


- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"DungeonSet";
}

- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

- (BOOL)writeToURL:(NSURL *)absoluteURL
            ofType:(NSString *)typeName 
             error:(NSError **)outError{
    NSMutableDictionary *dataToSave = 
        [NSMutableDictionary dictionaryWithCapacity: 3];
    [dataToSave setObject: dataVersion forKey: kDungeonDataVersion];
    [dataToSave setObject: dungeonAreasArray
                   forKey: kDungeonAreaArray];
    [dataToSave setObject:dungeonNotes forKey:kDungeonNotes];
    
    return [NSKeyedArchiver archiveRootObject:dataToSave
                                       toFile:[absoluteURL path]];
}

-(BOOL)readFromURL:(NSURL *)absoluteURL
            ofType:(NSString *)typeName
             error:(NSError **)outError{
    
    NSDictionary *restoredData = [NSKeyedUnarchiver 
                             unarchiveObjectWithFile:[absoluteURL path]];
    if (!restoredData) {
        return NO; //Could not open file
    }
    
    NSNumber *restoredVersion = [restoredData objectForKey:kDungeonDataVersion];
    BOOL versionCheckResult = YES;
    if (restoredVersion == nil) {
        versionCheckResult = [self displayVersioningErrorWithMessage:
                                @"The file you are attempting to open does not"
                              " contain a valid version number. Would you like"
                              "to try opening it anyway?"];
        
    } else {
        NSComparisonResult result = [dataVersion compare:restoredVersion];
        if (result == NSOrderedAscending) {
            versionCheckResult = [self displayVersioningErrorWithMessage:
                                  @"The file you are attempting to open was"
                                  " created with a newer version of Game Master"
                                  " and may not open properly in this version."
                                  " Would you like to try opening it anyway?"];
        } else if (result == NSOrderedDescending) {
            versionCheckResult = [self displayVersioningErrorWithMessage:
                                  @"The file you are attempting to open was"
                                  " created with an older version of Game Master"
                                  " and may not open properly in this version."
                                  " Would you like to try opening it anyway?"];
        }
    }

    
    if (versionCheckResult == NO) {
        return NO;
    }

    [dungeonAreasArray release];
    [dungeonNotes release];
    dungeonAreasArray = [[restoredData objectForKey:kDungeonAreaArray] retain];
    dungeonNotes = [[restoredData objectForKey:kDungeonNotes] retain];
    [dungeonAreaTableView reloadData];
    
    
    return YES;
}

+(BOOL) autosavesInPlace{
    return YES;
}


#pragma mark -
#pragma mark NSUserInterfaceValidations

-(BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem  {
    SEL theAction = [anItem action];
    
    if (theAction == @selector(removeSelectedItemFromAreaList:)) {
        if ([dungeonAreaTableView selectedRow] == -1) {
            return NO;
        } else {
            return YES;
        }

    } else if (theAction == @selector(exportAsText:)){
        if ([dungeonAreasArray count] == 0) {
            return NO;
        } else {
            return YES;
        }

    }else {
        return [super validateUserInterfaceItem:anItem];
    }
}


@end
