//
//  DungeonArea.m
//  Game Master
//
//  Represents an area in a dungeon or map
//  Each area has a name, a description, a game master's notes
//  and an enemies list.
//
//  Created by Tevis Money on 7/11/11.
//  Copyright 2011 Tevis Money. All rights reserved.
//

#import "DungeonArea.h"

NSString * const kDungeonAreaName = @"kDungeonAreaName";
NSString * const kDungeonAreaDescription = @"kDungeonAreaDescription";
NSString * const kDungeonAreaGameMasterNotes = @"kDungeonAreaGameMasterNotes";
NSString * const kDungeonAreaEnemiesNotes = @"kDungeonAreaEnemiesNotes";
NSString * const kDungeonAreaEnemiesList = @"kDungeonAreaEnemiesList";


@implementation DungeonArea

@synthesize areaName;
@synthesize description;
@synthesize gameMasterNotes;
@synthesize enemiesNotes;
@synthesize enemiesList;


- (id)init{
    return [self initWithName:@"New Area"];
}

- (id)initWithName:(NSString *) newName{
    if (self = [super init]) {
        areaName = [newName retain];
        description = [[NSMutableAttributedString alloc ] initWithString:@""];
        gameMasterNotes = [[NSMutableAttributedString alloc] initWithString:@""];
        enemiesNotes = [[NSMutableAttributedString alloc] initWithString:@""];
        //enemiesList = [[NSMutableArray array] retain];
    }
    return self;
}

- (void)dealloc{
    [areaName release];
    [description release];
    [gameMasterNotes release];
    [enemiesNotes release];
    [enemiesList release];
    [super dealloc];
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:areaName forKey:kDungeonAreaName];
    [aCoder encodeObject:description forKey:kDungeonAreaDescription];
    [aCoder encodeObject:gameMasterNotes forKey:kDungeonAreaGameMasterNotes];
    [aCoder encodeObject:enemiesNotes forKey:kDungeonAreaEnemiesNotes];
    [aCoder encodeObject:enemiesList forKey:kDungeonAreaEnemiesList];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        areaName = [[aDecoder decodeObjectForKey:kDungeonAreaName] retain];
        description = 
            [[aDecoder decodeObjectForKey:kDungeonAreaDescription] retain];
        gameMasterNotes = 
            [[aDecoder decodeObjectForKey:kDungeonAreaGameMasterNotes] retain];
        enemiesNotes = 
            [[aDecoder decodeObjectForKey:kDungeonAreaEnemiesNotes] retain];
        enemiesList = 
            [[aDecoder decodeObjectForKey:kDungeonAreaEnemiesList] retain];
    }
    return self;
}

+ (id)dungeonAreaWithName:(NSString *)newName {
    
    return [[[DungeonArea alloc] initWithName:newName] autorelease];
}


@end
