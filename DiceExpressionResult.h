//
//  DiceExpressionResult.h
//  Game Master
//
//  Created by Tevis Money on 2/24/12.
//  Copyright (c) 2012 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiceExpressionResult : NSObject{
        NSArray *workStack;
        NSNumber *result;
}

@property(retain) NSArray *workStack;
@property(retain) NSNumber *result;

+ (id) resultWithValue:(NSNumber *) value workStack:(NSMutableArray *) stack;

@end
