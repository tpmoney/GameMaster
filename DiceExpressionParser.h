//
//  DiceExpressionParser.h
//  Game Master
//
//  Created by Tevis Money on 2/23/12.
//  Copyright (c) 2012 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DiceExpressionResult.h"

@interface DiceExpressionParser : NSObject{
    NSRegularExpression *openParenthesis;
    NSRegularExpression *closeParenthesis;
    NSRegularExpression *number;
    NSRegularExpression *operatorChar;
}

@property (retain) NSRegularExpression *openParenthesis;
@property (retain) NSRegularExpression *closeParenthesis;
@property (retain) NSRegularExpression *number;
@property (retain) NSRegularExpression *operatorChar;

+ (id) expressionParser;

- (DiceExpressionResult *) evaluteExpression:(NSString *)expression showDice:(BOOL) dice showWork:(BOOL) work;

@end
