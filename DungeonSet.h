//
//  DungeonSet.h
//  Game Master
//
//  Created by Tevis Money on 10/30/10.
//  Copyright 2010 Apple. All rights reserved.
//


#import <Cocoa/Cocoa.h>
#import "DiceRollerController.h"

@interface DungeonSet : NSDocument <NSUserInterfaceValidations, 
    NSSplitViewDelegate>
{
    IBOutlet NSTableView *dungeonAreaTableView;
    IBOutlet NSTextField *newAreaNameTextField;
    IBOutlet NSTableColumn *areaNameColumn;
    IBOutlet NSTextView *descriptionTextView;
    IBOutlet NSTextView *gameMasterNotesTextView;
    IBOutlet NSTextView *enemiesNotesTextView;
    IBOutlet NSArrayController *dungeonAreaController;
    IBOutlet NSView *contentView;
    
    NSMutableArray *dungeonAreasArray;      //Array of dungeon areas in this file
    NSAttributedString *dungeonNotes;       //Notes for this particular dungeon
    NSNumber *dataVersion;                  //Version number for this document
    
}

- (IBAction)addNewItemToAreaList:(id)sender;

- (IBAction)removeSelectedItemFromAreaList:(id)sender;

- (IBAction)exportAsText:(id)sender;

- (IBAction)didChangeAreaName:(id)sender;

@end

