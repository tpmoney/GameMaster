//
//  DiceRollerController.m
//  Game Master
//
//  Created by Tevis Money on 2/21/12.
//  Copyright (c) 2012 Apple. All rights reserved.
//

#import "DiceRollerController.h"
#import "DiceExpressionParser.h"

@implementation DiceRollerController

@synthesize showWork;
@synthesize showDice;

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {

    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    showWork = NO;
    showDice = NO;
    [input becomeFirstResponder];
}



- (IBAction)roll:(id)sender{
    NSTextStorage *output = [results textStorage];
    
    if (showWork) {
        [output appendAttributedString:[[[NSAttributedString alloc] initWithString:@"\n------------------New Dice Roll-------------------"] autorelease]];

    }
    
    DiceExpressionParser *parser = [DiceExpressionParser expressionParser];
    DiceExpressionResult *result = [parser evaluteExpression:[input stringValue] showDice:self.showDice showWork:self.showWork];
    
    if (showDice || showWork) {
        for (NSString *work in [result workStack]){
            [output appendAttributedString:[[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", work]]autorelease]];
        }
    }
    
    if ([result result] == nil) {
        [[results textStorage] appendAttributedString:[[[NSAttributedString alloc] 
                                                       initWithString:@"\nError Parsing the Roll"] autorelease]];
    } else {
        [[results textStorage] appendAttributedString:[[[NSAttributedString alloc] initWithString:
                                                        [NSString stringWithFormat:@"\nTotal: %@\n", [[result result]stringValue]]]autorelease]];
    }
    [input becomeFirstResponder];
    [results scrollToEndOfDocument:sender];
    [results setNeedsDisplay:YES];
    
}

- (IBAction)toggleShowWork:(id)sender{
    showWork = !showWork;
    if (showWork) {
        showDice = YES;
        [showDiceCheck setState:NSOnState];
    }
}

- (IBAction)toggleShowDice:(id)sender{
    showDice = !showDice;
    if  (!showDice){
        showWork = NO;
        [showWorkCheck setState:NSOffState];
    }
}

@end
