//
//  DiceExpressionParser.m
//  Game Master
//
//  Created by Tevis Money on 2/23/12.
//  Copyright (c) 2012 Apple. All rights reserved.
//

#import "DiceExpressionParser.h"
#import <stdlib.h>


@implementation DiceExpressionParser

@synthesize openParenthesis;
@synthesize closeParenthesis;
@synthesize number;
@synthesize operatorChar;

NSInteger const INITIAL_STACK_SIZE = 5;   //Initial stack size for parser, shouldn't need to be increased

//Operator precedence
NSInteger const OPERATOR_ADD_SUBTRACT = 0;
NSInteger const OPERATOR_MULTIPLY_DIVIDE = 1;
NSInteger const OPERATOR_DICE = 2;

//Instance objects
NSMutableArray *workStack;
BOOL showDice;
BOOL showWork;

- (id)init{
    self = [super init];
    if (self) {
        NSError *error = NULL;
        
        openParenthesis = [[NSRegularExpression alloc] initWithPattern:@"\\(" options:0 error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
        
        closeParenthesis = [[NSRegularExpression alloc] initWithPattern:@"\\)" options:0 error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
        
        number = [[NSRegularExpression alloc] initWithPattern:@"\\d+" options:0 error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
        
        operatorChar = [[NSRegularExpression alloc] initWithPattern:@"\\+|\\-|\\*|\\/|d|D" options:0 error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
    }
    
    return self;
}

+ (id) expressionParser{
    return [[[self alloc] init] autorelease];
}

- (NSInteger) identifyOperatorType:(char) op{
    switch (op) {
        case '+':
        case '-':
            return OPERATOR_ADD_SUBTRACT;
            break;
        case '*':
        case '/':
            return OPERATOR_MULTIPLY_DIVIDE;
            break;
        case 'd':
        case 'D':
            return OPERATOR_DICE;
        default:
            return -1;
            if (showWork) {
                [workStack addObject:[NSString stringWithFormat:@"Tried to identify unknown operator: %c", op]];
            }
            break;
    }
}

-(NSNumber *) rollDice:(NSNumber *)numberOfDice withSides:(NSNumber *) numberOfSides{
    
    int result = 0; //The total result of this dice expression    
    NSMutableArray *diceRolls = [[NSMutableArray alloc] initWithCapacity:[numberOfDice intValue]]; //Storage for individual results

    for (int cv = 0; cv < [numberOfDice intValue]; cv++) {
        int rollResult = arc4random_uniform([numberOfSides intValue]) + 1; 
        result += rollResult; 
        [diceRolls addObject:[NSString stringWithFormat:@"[%i]  ", rollResult]]; 
    }
    
    //Generate the work string for this roll
    if (showDice) {
        NSString *workString = @"";
        
        //Add the individual rolls
        for (NSString *rollResult in diceRolls){
            workString = [workString stringByAppendingString:rollResult];
        }
        
        //Add the total
        NSString *totalString = [NSString stringWithFormat:@"  Total: %i", result];
        workString = [workString stringByAppendingString:totalString];
        
        //Put it all together
        workString = [NSString stringWithFormat:@"Dice Roll: %id%i : %@", [numberOfDice intValue], [numberOfSides intValue], workString];
        [workStack addObject:workString];
    }
    [diceRolls release];
    return [NSNumber numberWithInt:result];
}

-(NSNumber *) collapseExpressionWithFirstOperand:(NSNumber *) firstOperand 
                                       operation:(NSString *) operation 
                                   secondOperand:(NSNumber *) secondOperand{
   
    char op = [operation characterAtIndex:0];
    NSInteger result;
    switch (op) {
        case '+':
            result = [firstOperand intValue] + [secondOperand intValue];
            
            //Log the result
            if (showWork) {
                [workStack addObject:[NSString stringWithFormat:@"Addition: %i + %i = %ld", [firstOperand intValue], [secondOperand intValue], result]];
            }
            
            return [NSNumber numberWithInt:result];
            break;
            
        case '-':
            result = [firstOperand intValue] - [secondOperand intValue];
            
            //Log the result
            if (showWork) {
                [workStack addObject:[NSString stringWithFormat:@"Subtraction: %i - %i = %ld", [firstOperand intValue], [secondOperand intValue], result]];
            }
            
            return [NSNumber numberWithInt:result];
            break;
            
        case '*':
            result = [firstOperand intValue] * [secondOperand intValue];
            
            //Log the result
            if (showWork) {
                [workStack addObject:[NSString stringWithFormat:@"Multiplication: %i * %i = %ld", [firstOperand intValue], [secondOperand intValue], result]];
            }
            
            return [NSNumber numberWithInt:result];
            break;
            
        case '/':
            result = [firstOperand intValue] / [secondOperand intValue];

            //Log the result
            if (showWork) {
                [workStack addObject:[NSString stringWithFormat:@"Division: %i / %i = %ld", [firstOperand intValue], [secondOperand intValue], result]];
            }
            
            return [NSNumber numberWithInt:result];
            break;
            
        case 'd':
        case 'D':
            return [self rollDice: firstOperand withSides: secondOperand];
            break;
            
        default:
            return nil;
            break;
    }
}

- (NSNumber *) collapseStack:(NSMutableArray *) expressionStack{
    //Get the top element on the stack, which is an operand
    NSNumber *secondOperand = [[[expressionStack lastObject] retain] autorelease];
    [expressionStack removeLastObject];
    
    //Get the top element on the stack, which is an operation
    NSString *operation = [[[expressionStack lastObject] retain] autorelease];
    [expressionStack removeLastObject];
    
    //Get the top element on the stack, which is an operand
    NSNumber *firstOperand = [[[expressionStack lastObject] retain] autorelease];
    [expressionStack removeLastObject];
    
    NSNumber *result = [self collapseExpressionWithFirstOperand:firstOperand 
                                                      operation:operation 
                                                  secondOperand:secondOperand];
    return result;
}

- (BOOL) canProceedWithCollapseOnExpression:(NSString *)expression 
                                  fromIndex:(NSInteger)index 
                 withLastOperatorPrecedence:(NSInteger)lastOperator{
    NSRange nextOpLocation = [operatorChar rangeOfFirstMatchInString:expression options:0 range:NSMakeRange(index, [expression length] - index)];
    char nextOp = [expression characterAtIndex:nextOpLocation.location];
    NSInteger nextOpPrecedence = [self identifyOperatorType:nextOp];
    if (lastOperator >= nextOpPrecedence) {
        return YES;
    } else {
        return NO;
    }
}

- (NSNumber *) parseExpression:(NSString *)expression{
    NSMutableArray *expressionStack = [NSMutableArray arrayWithCapacity:6];
    NSInteger lastOperator = -1;
    int index;
    
    //Walk through the expression, looking for tokens
    for(index = 0; index < [expression length];){
        
        //Try to collapse the stack
        while ([expressionStack count] >= 3 &&                            // If we have at least 3 items
               [expressionStack count] % 2 == 1 &&                        // And there are an odd number of items
               [self canProceedWithCollapseOnExpression:expression        //  on the stack
                                              fromIndex:index             // And we can proceed because there are no
                             withLastOperatorPrecedence:lastOperator] ) { //  higher order operations pending
                   
                   
                   NSNumber *result = [self collapseStack:expressionStack];
                   
                   
                   if([expressionStack count] >=2){
                       NSString *lastOpToken = [expressionStack lastObject];
                       lastOperator = [self identifyOperatorType:[lastOpToken characterAtIndex:0]];
                   }
                   [expressionStack addObject:result];
               }
        
        
    
        
        if([number numberOfMatchesInString:expression options:0 range:NSMakeRange(index, 1)]){

            //Handle number tokens

            NSRange tokenRange = [number rangeOfFirstMatchInString:expression options:0 range:NSMakeRange(index, [expression length] - index)];
            NSInteger token = [[expression substringWithRange:tokenRange] intValue];
            [expressionStack addObject:[NSNumber numberWithInt:token]];
            
            index = index + tokenRange.length; // Set index to the next position after this token
        
        } else if ([operatorChar numberOfMatchesInString:expression options:0 range:NSMakeRange(index, 1)]){
            
            //Handle operator tokens
            
            char op = [expression characterAtIndex:index];
            
            if ([expressionStack count] % 2 == 0) {
                if (showWork) {
                    [workStack addObject:[NSString stringWithFormat:@"Encountered an operator token (%c) at an invalid location: skipping token.", op]];
                }
                index++;
                continue;
            }
            
            lastOperator = [self identifyOperatorType:op];
            
            
            
            [expressionStack addObject:[expression substringWithRange:NSMakeRange(index, 1)]];
           
            index++; //Set index to the next position after this token
            
        } else if ([openParenthesis numberOfMatchesInString:expression options:0 range:NSMakeRange(index, 1)]){
            
            //Handle parenthesis
            
            NSInteger indexOfClose = index;
            NSInteger indexOfNextOpen = index+1;
            do{
                //Ensure we always increment to avoid infinite loops
                indexOfClose ++;
                indexOfNextOpen ++;
                
                //Get the index of the next close parenthisis
                indexOfClose = [closeParenthesis rangeOfFirstMatchInString:expression options:0 range:NSMakeRange(indexOfClose, [expression length] - indexOfClose)].location;
                
                //If we found an open parenthisis with no close, that is an error
                if (indexOfClose == NSNotFound) {
                    if (showWork) {
                        [workStack addObject:@"Error parsing parenthises, unable to find close parenthisis"];
                    }
                    return nil;
                }
                                    
                //Get the index of the next open parenthisis
                indexOfNextOpen = [openParenthesis rangeOfFirstMatchInString:expression options:0 range:NSMakeRange(indexOfNextOpen, [expression length] - indexOfNextOpen)].location;
                    
            } while (indexOfNextOpen != NSNotFound || indexOfClose > indexOfNextOpen);
            
            if (showWork) {
                [workStack addObject:[NSString stringWithFormat:@"--- Begining Evaluation of Sub-Expression: \"%@\" ---", [expression substringWithRange:NSMakeRange(index, indexOfClose - index+1)]]];
            }
            
            NSNumber *token = [self parseExpression:[expression substringWithRange:
                                                     NSMakeRange(index+1, indexOfClose-index-1)]];
  
            if(token == nil){

                if (showWork) {
                    [workStack addObject:[NSString stringWithFormat:@"Result from sub expression \"%@\" resulted in an error.", [expression substringWithRange:NSMakeRange(index, indexOfClose-index)]]];
                }
                
                return nil;
            } else {          
                
                if (showWork) {
                    [workStack addObject:[NSString stringWithFormat:@"--- Ending Evaluation of Sub-Expression, Result: %i ---", [token intValue]]];
                }
                
                [expressionStack addObject:token];
            }
            
            index = indexOfClose + 1; // Set index to the next position after this token
        } else {
            index++;
        }
        
       
    }
    
    while ([expressionStack count] >=3 && [expressionStack count]%2 == 1) {
        [expressionStack addObject:[self collapseStack:expressionStack]];
    }
    
    if ([expressionStack count] == 1) {
        return [expressionStack lastObject];
    } else {
        
        if (showWork) {
            [workStack addObject:[NSString stringWithFormat:@"Reached the end of expression, exression stack still has items with size: %lu", [expressionStack count]]];
            [workStack addObject:[NSString stringWithFormat:@"Stack is: %@", expressionStack]];
            [workStack addObject:[NSString stringWithFormat:@"Expression was: %@", expression]];
        }
        
        return nil;
    }
                 
}




- (DiceExpressionResult *) evaluteExpression:(NSString *)expression showDice:(BOOL)dice showWork:(BOOL)work{
    workStack = [[NSMutableArray alloc] initWithCapacity:5];
    showDice = dice;
    showWork = work;
   
    NSNumber *result = [self parseExpression:expression];
    DiceExpressionResult *expressionResult = [DiceExpressionResult resultWithValue:result workStack:workStack];
    
    [workStack release];
    return expressionResult;
}

                 
- (void) dealloc{
    self.openParenthesis = nil;
    self.closeParenthesis = nil;
    self.number = nil;
    self.operatorChar = nil;
    [super dealloc];
}

@end
