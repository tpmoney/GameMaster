//
//  DungeonArea.h
//  Game Master
//
//  Represents an area in a dungeon or map
//  Each area has a name, a description, a game master's notes
//  and an enemies list.
//
//  Created by Tevis Money on 7/11/11.
//  Copyright 2011 Tevis Money. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface DungeonArea : NSObject <NSCoding>{
    NSString *areaName;
    NSMutableAttributedString *description;
    NSMutableAttributedString *gameMasterNotes;
    NSMutableAttributedString *enemiesNotes;
    NSMutableArray *enemiesList;
}

- (id)initWithName:(NSString *)newName;
+ (id)dungeonAreaWithName:(NSString *)newName;

@property (retain) NSString *areaName;
@property (retain) NSMutableAttributedString *description;
@property (retain) NSMutableAttributedString *gameMasterNotes;
@property (retain) NSMutableAttributedString *enemiesNotes;
@property (retain) NSMutableArray *enemiesList;

@end
