//
//  GameMasterController.m
//  Game Master
//
//  Created by Tevis Money on 7/19/11.
//  Copyright 2011 Apple. All rights reserved.
//

#import "GameMasterController.h"


@implementation GameMasterController

- (id) init
{
    self = [super init];
    if (self != nil) {
        applicationHasStarted = NO;
    }
    return self;
}

- (IBAction)showDiceRoller:(id)sender{
    if (!roller) {
        roller = [[DiceRollerController alloc] initWithWindowNibName:@"DiceRoller"];
    }
    [roller showWindow:sender];
}

- (void) dealloc
{
    [super dealloc];
}


#pragma mark -
#pragma mark NSApplicationDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification{
    applicationHasStarted = YES;
}

/*
 * From Cocoa With Love
 * http://cocoawithlove.com/2008/05/open-previous-document-on-application.html
 */
- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender
{
    // On startup, when asked to open an untitled file, open the last opened
    // file instead
    if (!applicationHasStarted)
    {
        // Get the recent documents
        NSDocumentController *controller =
        [NSDocumentController sharedDocumentController];
        NSArray *documents = [controller recentDocumentURLs];
        
        // If there is a recent document, try to open it.
        if ([documents count] > 0)
        {
            NSError *error = nil;
            [controller
             openDocumentWithContentsOfURL:[documents objectAtIndex:0]
             display:YES error:&error];
            
            // If there was no error, then prevent untitled from appearing.
            if (error == nil)
            {
                return NO;
            }
        }
    }
    
    return YES;
}


@end
