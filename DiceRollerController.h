//
//  DiceRollerController.h
//  Game Master
//
//  Created by Tevis Money on 2/21/12.
//  Copyright (c) 2012 Apple. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DiceRollerController : NSWindowController {
    
    IBOutlet NSTextView *results;
    IBOutlet NSTextField *input;
    IBOutlet NSButton *rollDice;
    IBOutlet NSButton *showDiceCheck;
    IBOutlet NSButton *showWorkCheck;
    BOOL showWork;
    BOOL showDice;
}

@property(assign) BOOL showWork;
@property(assign) BOOL showDice;

- (IBAction)roll:(id)sender;

- (IBAction)toggleShowWork:(id)sender;

- (IBAction)toggleShowDice:(id)sender;

@end
